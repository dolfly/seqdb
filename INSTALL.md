SeqDB Installation
==================

We have only tested SeqDB on Mac OS X 10.7/10.8 and CentOS 5.4/6.3, but we have
made an effort to stick to portable/POSIX standards and believe it will work on
most flavors of UNIX.

You will need a recent (>= 1.8) version of the HDF5 library as well as a
C++ compiler (we recommend g++ >= 4.4).


Mac OS X
--------

An easy way to install the HDF5 prerequisite is through Homebrew:

 https://github.com/mxcl/homebrew/wiki/installation

Once Homebrew is installed, run:

 brew install hdf5

To obtain a compiler, you can install the command-line developer tools
available here (you will need to register for an Apple ID):

 https://developer.apple.com/downloads

Now jump to "Building from source" below.


Redhat/Fedora/CentOS
--------------------

You can install the prerequisites with yum:

 sudo yum install hdf5 hdf5-dev

Now jump to "Building from source" below.


Building from source
--------------------

These instructions assume you are installing to the default location
(/usr/local) which may require root access. Alternative instructions for
installing to you home directory are available in the next section.

Download the latest tarball from:

 https://github.com/mhowison/seqdb/downloads

Unpack the tarball:

 tar xf seqdb-X.X.X.tar.gz
 cd seqdb-X.X.X

Run the configure script:

 ./configure

Alternatively, if your HDF5 library is installed in a non-standard location,
you will need to specify the full path to it:

 ./configure --with-hdf5=<path/to/hdf5>

Build the executables with:

 make

Then install using sudo:

 sudo make install

Try running the 'seqdb' wrapper script to print a help message:

 seqdb help

Follow the TUTORIAL to test out your installation.


Installing to your home directory
---------------------------------

To install to an alternative location, like your home directory, use the
--prefix option when configuring:

 ./configure --prefix=$HOME

If you haven't already added your $HOME/bin directory to your path, you will
need to do this in one of your initialization scripts. For bash, you could do:

 echo "export PATH=$HOME/bin:$PATH" >>~/.bashrc
 source ~/.bashrc

Now the seqdb wrapper and executables should be in your path.

