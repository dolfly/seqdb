diff --git a/scripts/seqdb.in b/scripts/seqdb.in
index 2ec5777..4014b17 100755
--- a/scripts/seqdb.in
+++ b/scripts/seqdb.in
@@ -27,6 +27,8 @@ prefix=@prefix@
 exec_prefix=@exec_prefix@
 bindir=@bindir@
 
+PATH=$bindir:$PATH
+
 usage() {
 	echo "
 usage: seqdb COMMAND [ARGS]
@@ -57,13 +59,43 @@ seqdb-cite() {
 	echo "
 Please cite SeqDB as:
 
-[publication under review]
+M. Howison, \"High-throughput compression of FASTQ data with SeqDB,\"
+IEEE/ACM Transactions on Computational Biology and Bioinformatics, (in press).
+[http://ccv.brown.edu/mhowison/Howison_SeqDB_TCCB_2012.pdf]
 "
 	exit 0
 }
 
 seqdb-version() {
-	echo "SeqDB @PACKAGE_VERSION@"
+	echo "
+SeqDB @PACKAGE_VERSION@
+
+Built on system @PACKAGE_BUILD_HOST@ (@PACKAGE_BUILD_HOST_OS@)
+at @PACKAGE_BUILD_DATE@
+Compiler: @PACKAGE_COMPILER@
+Options: @PACKAGE_COMPILE_OPTIONS@
+
+Copyright 2011-2012, Brown University, Providence, RI.
+
+                        All Rights Reserved
+
+Permission to use, copy, modify, and distribute this software and its
+documentation for any purpose other than its incorporation into a
+commercial product is hereby granted without fee, provided that the
+above copyright notice appear in all copies and that both that
+copyright notice and this permission notice appear in supporting
+documentation, and that the name of Brown University not be used in
+advertising or publicity pertaining to distribution of the software
+without specific, written prior permission.
+
+BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
+INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
+PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
+ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
+WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
+ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
+OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
+"
 	exit 0
 }
 
@@ -90,9 +122,9 @@ then
 		fi
 	elif [ $matches == "mount" ]
 	then
-		"$bindir/seqdb-$matches" "$@" &
+		"seqdb-$matches" "$@" &
 	else
-		"$bindir/seqdb-$matches" "$@"
+		"seqdb-$matches" "$@"
 	fi
 else
 	echo "unrecognized command: $1 (? ${matches[@]})"
diff --git a/src/Makefile.am b/src/Makefile.am
index 35fc691..ace4c52 100644
--- a/src/Makefile.am
+++ b/src/Makefile.am
@@ -1,16 +1,31 @@
-lib_LIBRARIES = libseqdb.a
-bin_PROGRAMS = seqdb-compress seqdb-extract seqdb-profile seqdb-mount
+# library
+
+lib_LTLIBRARIES = libseqdb.la
 
-libseqdb_a_SOURCES = \
+libseqdb_la_SOURCES = \
 	seq.cpp seqpack.cpp seqdb.cpp h5seqdb.cpp fastq.cpp \
 	blosc_filter.c blosc.c blosclz.c shuffle.c \
 	seq.h seqpack.h seqdb.h h5seqdb.h fastq.h util.h \
 	blosc_filter.h blosc.h blosclz.h shuffle.h
 
-LDADD = libseqdb.a
+# programs
+
+bin_PROGRAMS = seqdb-compress seqdb-extract seqdb-profile seqdb-mount
+
+LDADD = libseqdb.la
 
 seqdb_compress_SOURCES = seqdb-compress.cpp
 seqdb_extract_SOURCES = seqdb-extract.cpp
 seqdb_profile_SOURCES = seqdb-profile.cpp
 seqdb_mount_SOURCES = seqdb-mount.cpp
 
+# includes
+
+library_includedir = $(includedir)/seqdb
+library_include_HEADERS = \
+	seq.h seqpack.h seqdb.h h5seqdb.h fastq.h seqdb-version.h
+
+# pkgconfig
+
+pkgconfigdir = $(libdir)/pkgconfig
+pkgconfig_DATA = seqdb.pc
diff --git a/src/blosc.c b/src/blosc.c
index 50cb962..233acea 100644
--- a/src/blosc.c
+++ b/src/blosc.c
@@ -8,6 +8,11 @@
 **********************************************************************/
 
 
+#if HAVE_PTHREAD_SETAFFINITY_NP
+#define BLOSC_PIN_THREADS 1
+#define _GNU_SOURCE    /* Needed for thread pinning */
+#endif
+
 #include <stdlib.h>
 #include <stdio.h>
 #include <string.h>
@@ -34,6 +39,9 @@
   #include "win32/pthread.c"
 #else
   #include <pthread.h>
+#if BLOSC_PIN_THREADS
+  #include <sched.h>
+#endif
 #endif
 
 
@@ -59,7 +67,7 @@ int pid = 0;                    /* the PID for this process */
 /* Global variables for threads */
 int32_t nthreads = 1;            /* number of desired threads in pool */
 int32_t init_threads_done = 0;   /* pool of threads initialized? */
-int32_t end_threads = 0;         /* should exisiting threads end? */
+int32_t end_threads = 0;         /* should existing threads end? */
 int32_t init_sentinels_done = 0; /* sentinels initialized? */
 int32_t giveup_code;             /* error code when give up */
 int32_t nblock;                  /* block counter */
@@ -69,11 +77,13 @@ int32_t tids[BLOSC_MAX_THREADS];       /* ID per each thread */
 pthread_attr_t ct_attr;          /* creation time attributes for threads */
 #endif
 
-#if defined(_POSIX_BARRIERS) && (_POSIX_BARRIERS - 20012L) >= 0
+/* Have problems using posix barriers when symbol value is 200112L */
+/* This requires more investigation, but will work for the moment */
+#if defined(_POSIX_BARRIERS) && ( (_POSIX_BARRIERS - 20012L) >= 0 && _POSIX_BARRIERS != 200112L)
 #define _POSIX_BARRIERS_MINE
 #endif
 
-/* Syncronization variables */
+/* Synchronization variables */
 pthread_mutex_t count_mutex;
 #ifdef _POSIX_BARRIERS_MINE
 pthread_barrier_t barr_init;
@@ -169,7 +179,7 @@ uint8_t *my_malloc(size_t size)
 
 #if defined(_WIN32)
   /* A (void *) cast needed for avoiding a warning with MINGW :-/ */
-  block	=   (void *)_aligned_malloc(size, 16);
+  block = (void *)_aligned_malloc(size, 16);
 #elif defined __APPLE__
   /* Mac OS X guarantees 16-byte alignment in small allocs */
   block = malloc(size);
@@ -182,7 +192,7 @@ uint8_t *my_malloc(size_t size)
 
   if (block == NULL || res != 0) {
     printf("Error allocating memory!");
-    exit(1);
+    return NULL;
   }
 
   return (uint8_t *)block;
@@ -259,8 +269,8 @@ static int blosc_c(uint32_t blocksize, int32_t leftoverblock,
   neblock = blocksize / nsplits;
   for (j = 0; j < nsplits; j++) {
     dest += sizeof(int32_t);
-    ntbytes += sizeof(int32_t);
-    ctbytes += sizeof(int32_t);
+    ntbytes += (uint32_t)sizeof(int32_t);
+    ctbytes += (int32_t)sizeof(int32_t);
     maxout = neblock;
     if (ntbytes+maxout > maxbytes) {
       maxout = maxbytes - ntbytes;   /* avoid buffer overrun */
@@ -329,7 +339,7 @@ static int blosc_d(uint32_t blocksize, int32_t leftoverblock,
   for (j = 0; j < nsplits; j++) {
     cbytes = sw32(((uint32_t *)(src))[0]);   /* amount of compressed bytes */
     src += sizeof(int32_t);
-    ctbytes += sizeof(int32_t);
+    ctbytes += (int32_t)sizeof(int32_t);
     /* Uncompress */
     if (cbytes == neblock) {
       memcpy(_tmp, src, neblock);
@@ -460,7 +470,7 @@ int parallel_blosc(void)
 
 
 /* Convenience functions for creating and releasing temporaries */
-void create_temporaries(void)
+int create_temporaries(void)
 {
   int32_t tid;
   uint32_t typesize = params.typesize;
@@ -468,12 +478,21 @@ void create_temporaries(void)
   /* Extended blocksize for temporary destination.  Extended blocksize
    is only useful for compression in parallel mode, but it doesn't
    hurt serial mode either. */
-  uint32_t ebsize = blocksize + typesize*sizeof(int32_t);
+  uint32_t ebsize = blocksize + typesize*(uint32_t)sizeof(int32_t);
 
   /* Create temporary area for each thread */
   for (tid = 0; tid < nthreads; tid++) {
-    params.tmp[tid] = my_malloc(blocksize);
-    params.tmp2[tid] = my_malloc(ebsize);
+    uint8_t *tmp = my_malloc(blocksize);
+    uint8_t *tmp2;
+    if (tmp == NULL) {
+      return -1;
+    }
+    params.tmp[tid] = tmp;
+    tmp2 = my_malloc(ebsize);
+    if (tmp2 == NULL) {
+      return -1;
+    }
+    params.tmp2[tid] = tmp2;
   }
 
   init_temps_done = 1;
@@ -481,6 +500,7 @@ void create_temporaries(void)
   current_temp.nthreads = nthreads;
   current_temp.typesize = typesize;
   current_temp.blocksize = blocksize;
+  return 0;
 }
 
 
@@ -505,13 +525,20 @@ int do_job(void) {
 
   /* Initialize/reset temporaries if needed */
   if (!init_temps_done) {
-    create_temporaries();
+    int ret = create_temporaries();
+    if (ret < 0) {
+      return -1;
+    }
   }
   else if (current_temp.nthreads != nthreads ||
            current_temp.typesize != params.typesize ||
            current_temp.blocksize != params.blocksize) {
+    int ret;
     release_temporaries();
-    create_temporaries();
+    ret = create_temporaries();
+    if (ret < 0) {
+      return -1;
+    }
   }
 
   /* Run the serial version when nthreads is 1 or when the buffers are
@@ -590,7 +617,7 @@ int32_t compute_blocksize(int32_t clevel, uint32_t typesize, int32_t nbytes)
 
 /* The public routine for compression.  See blosc.h for docstrings. */
 int blosc_compress(int clevel, int doshuffle, size_t typesize, size_t nbytes,
-		   const void *src, void *dest, size_t destsize)
+      const void *src, void *dest, size_t destsize)
 {
   uint8_t *_dest=NULL;         /* current pos for destination buffer */
   uint8_t *flags;              /* flags for header.  Currently booked:
@@ -608,10 +635,11 @@ int blosc_compress(int clevel, int doshuffle, size_t typesize, size_t nbytes,
   /* Check buffer size limits */
   if (nbytes > BLOSC_MAX_BUFFERSIZE) {
     /* If buffer is too large, give up. */
-    fprintf(stderr, "Input buffer size cannot exceed %d MB\n",
-            BLOSC_MAX_BUFFERSIZE / MB);
-    exit(1);
+    fprintf(stderr, "Input buffer size cannot exceed %d bytes\n",
+            BLOSC_MAX_BUFFERSIZE);
+    return -1;
   }
+
   /* We can safely do this assignation now */
   nbytes_ = (uint32_t)nbytes;
 
@@ -691,6 +719,9 @@ int blosc_compress(int clevel, int doshuffle, size_t typesize, size_t nbytes,
   if (!(*flags & BLOSC_MEMCPYED)) {
     /* Do the actual compression */
     ntbytes = do_job();
+    if (ntbytes < 0) {
+      return -1;
+    }
     if ((ntbytes == 0) && (nbytes_+BLOSC_MAX_OVERHEAD <= maxbytes)) {
       /* Last chance for fitting `src` buffer in `dest`.  Update flags
        and do a memcpy later on. */
@@ -709,6 +740,9 @@ int blosc_compress(int clevel, int doshuffle, size_t typesize, size_t nbytes,
        cache size or multi-cores */
       params.ntbytes = BLOSC_MAX_OVERHEAD;
       ntbytes = do_job();
+      if (ntbytes < 0) {
+	return -1;
+      }
     }
     else {
       memcpy((uint8_t *)dest+BLOSC_MAX_OVERHEAD, src, nbytes_);
@@ -784,6 +818,9 @@ int blosc_decompress(const void *src, void *dest, size_t destsize)
       /* More effective with large buffers that are multiples of the
        cache size or multi-cores */
       ntbytes = do_job();
+      if (ntbytes < 0) {
+	return -1;
+      }
     }
     else {
       memcpy(dest, (uint8_t *)src+BLOSC_MAX_OVERHEAD, nbytes);
@@ -793,6 +830,9 @@ int blosc_decompress(const void *src, void *dest, size_t destsize)
   else {
     /* Do the actual decompression */
     ntbytes = do_job();
+    if (ntbytes < 0) {
+      return -1;
+    }
   }
 
   assert(ntbytes <= (int32_t)destsize);
@@ -859,7 +899,13 @@ int blosc_getitem(const void *src, int start, int nitems, void *dest)
   /* Initialize temporaries if needed */
   if (tmp == NULL || tmp2 == NULL || current_temp.blocksize < blocksize) {
     tmp = my_malloc(blocksize);
+    if (tmp == NULL) {
+      return -1;
+    }
     tmp2 = my_malloc(blocksize);
+    if (tmp2 == NULL) {
+      return -1;
+    }
     tmp_init = 1;
   }
 
@@ -889,7 +935,7 @@ int blosc_getitem(const void *src, int start, int nitems, void *dest)
     if (flags & BLOSC_MEMCPYED) {
       /* We want to memcpy only */
       memcpy((uint8_t *)dest + ntbytes,
-	     (uint8_t *)src + BLOSC_MAX_OVERHEAD + j*blocksize + startb,
+          (uint8_t *)src + BLOSC_MAX_OVERHEAD + j*blocksize + startb,
              bsize2);
       cbytes = bsize2;
     }
@@ -965,7 +1011,7 @@ void *t_blosc(void *tids)
 
     /* Get parameters for this thread before entering the main loop */
     blocksize = params.blocksize;
-    ebsize = blocksize + params.typesize*sizeof(int32_t);
+    ebsize = blocksize + params.typesize*(uint32_t)sizeof(int32_t);
     compress = params.compress;
     flags = params.flags;
     maxbytes = params.maxbytes;
@@ -1120,6 +1166,17 @@ int init_threads(void)
   pthread_attr_setdetachstate(&ct_attr, PTHREAD_CREATE_JOINABLE);
 #endif
 
+#if BLOSC_PIN_THREADS
+  /* Create a cpuset for thread pinning */
+  size_t cpu_set_size = CPU_ALLOC_SIZE(BLOSC_MAX_THREADS);
+  cpu_set_t* cpu_set = CPU_ALLOC(BLOSC_MAX_THREADS);
+  if (cpu_set == NULL) {
+    fprintf(stderr, "ERROR from CPU_ALLOC()\n");
+    perror("CPU_ALLOC");
+    exit(-1);
+  }
+#endif
+
   /* Finally, create the threads in detached state */
   for (tid = 0; tid < nthreads; tid++) {
     tids[tid] = tid;
@@ -1133,8 +1190,24 @@ int init_threads(void)
       fprintf(stderr, "\tError detail: %s\n", strerror(rc));
       exit(-1);
     }
+    /* Pin threads */
+#if BLOSC_PIN_THREADS
+    CPU_ZERO_S(cpu_set_size, cpu_set);
+    CPU_SET_S(tid, cpu_set_size, cpu_set);
+    //fprintf(stderr, "blosc: pinning thread %lu to CPU %d\n", threads[tid], tid)
+    rc = pthread_setaffinity_np(threads[tid], cpu_set_size, cpu_set);
+    if (rc) {
+      fprintf(stderr, "ERROR; return code from pthread_setaffinity_np() is %d\n", rc);
+      fprintf(stderr, "\tError detail: %s\n", strerror(rc));
+      exit(-1);
+    }
+#endif
   }
 
+#if BLOSC_PIN_THREADS
+    CPU_FREE(cpu_set);
+#endif
+
   init_threads_done = 1;                 /* Initialization done! */
   pid = (int)getpid();                   /* save the PID for this process */
 
diff --git a/src/blosc.h b/src/blosc.h
index 25a37a0..bb91d58 100644
--- a/src/blosc.h
+++ b/src/blosc.h
@@ -14,11 +14,11 @@
 /* Version numbers */
 #define BLOSC_VERSION_MAJOR    1    /* for major interface/format changes  */
 #define BLOSC_VERSION_MINOR    1    /* for minor interface/format changes  */
-#define BLOSC_VERSION_RELEASE  3    /* for tweaks, bug-fixes, or development */
+#define BLOSC_VERSION_RELEASE  5    /* for tweaks, bug-fixes, or development */
 
-#define BLOSC_VERSION_STRING   "1.1.3"  /* string version.  Sync with above! */
-#define BLOSC_VERSION_REVISION "$Rev: 296 $"   /* revision version */
-#define BLOSC_VERSION_DATE     "$Date:: 2010-11-16 #$"    /* date version */
+#define BLOSC_VERSION_STRING   "1.1.5"  /* string version.  Sync with above! */
+#define BLOSC_VERSION_REVISION "$Rev$"   /* revision version */
+#define BLOSC_VERSION_DATE     "$Date:: 2012-09-21 #$"    /* date version */
 
 /* The *_VERS_FORMAT should be just 1-byte long */
 #define BLOSC_VERSION_FORMAT    2   /* Blosc format version, starting at 1 */
@@ -36,7 +36,7 @@
 #define BLOSC_MAX_OVERHEAD BLOSC_MIN_HEADER_LENGTH
 
 /* Maximum buffer size to be compressed */
-#define BLOSC_MAX_BUFFERSIZE INT_MAX   /* Signed 32-bit internal counters */
+#define BLOSC_MAX_BUFFERSIZE (INT_MAX - BLOSC_MAX_OVERHEAD)
 
 /* Maximum typesize before considering buffer as a stream of bytes */
 #define BLOSC_MAX_TYPESIZE 255         /* Cannot be larger than 255 */
@@ -61,7 +61,7 @@ extern "C" {
   between 0 (no compression) and 9 (maximum compression).
 
   `doshuffle` specifies whether the shuffle compression preconditioner
-  should be applyied or not.  0 means not applying it and 1 means
+  should be applied or not.  0 means not applying it and 1 means
   applying it.
 
   `typesize` is the number of bytes for the atomic type in binary
@@ -88,7 +88,7 @@ extern "C" {
  */
 
 int blosc_compress(int clevel, int doshuffle, size_t typesize, size_t nbytes,
-		   const void *src, void *dest, size_t destsize);
+                   const void *src, void *dest, size_t destsize);
 
 
 /**
diff --git a/src/blosc_filter.c b/src/blosc_filter.c
index d180056..3babbba 100644
--- a/src/blosc_filter.c
+++ b/src/blosc_filter.c
@@ -19,22 +19,28 @@
 #include "blosc.h"
 #include "blosc_filter.h"
 
-
-/* The conditional below is necessary because the THG team has decided
-  to fix an API inconsistency in the definition of the H5Z_class_t
-  structure in version 1.8.3 */
-#if H5_VERS_MAJOR == 1 && H5_VERS_MINOR == 8 && (H5_VERS_RELEASE < 3 || !H5_USE_16_API)
-/* 1.8.x where x >= 3 */
-#define H5Z_16API 0
-#define PUSH_ERR(func, minor, str) H5Epush1(__FILE__, func, __LINE__, H5E_PLINE, minor, str)
-#define GET_FILTER(a,b,c,d,e,f,g) H5Pget_filter_by_id2(a,b,c,d,e,f,g,NULL)
-
+#if H5Epush_vers == 2
+/* 1.8.x */
+#define PUSH_ERR(func, minor, str) H5Epush(H5E_DEFAULT, __FILE__, func, __LINE__, H5E_ERR_CLS, H5E_PLINE, minor, str)
 #else
 /* 1.6.x */
-#define H5Z_16API 1
 #define PUSH_ERR(func, minor, str) H5Epush(__FILE__, func, __LINE__, H5E_PLINE, minor, str)
+#endif
+
+#if H5Pget_filter_by_id_vers == 2
+/* 1.8.x */
+#define GET_FILTER(a,b,c,d,e,f,g) H5Pget_filter_by_id(a,b,c,d,e,f,g,NULL)
+#else
+/* 1.6.x */
 #define GET_FILTER H5Pget_filter_by_id
+#endif
 
+#if H5Z_class_t_vers == 2
+/* 1.8.x where x >= 3 */
+#define H5Z_16API 0
+#else
+/* 1.6.x and 1.8.x with x < 3*/
+#define H5Z_16API 1
 #endif
 
 size_t blosc_filter(unsigned flags, size_t cd_nelmts,
diff --git a/src/blosclz.c b/src/blosclz.c
index 66cf718..eb4e249 100644
--- a/src/blosclz.c
+++ b/src/blosclz.c
@@ -132,7 +132,7 @@ int blosclz_compress(int opt_level, const void* input,
     return 0;                   /* Mark this as uncompressible */
   }
 
-  htab = malloc(hash_size*sizeof(uint16_t));
+  htab = (uint16_t *) malloc(hash_size*sizeof(uint16_t));
 
   /* sanity check */
   if(BLOSCLZ_UNEXPECT_CONDITIONAL(length < 4)) {
@@ -453,7 +453,7 @@ int blosclz_decompress(const void* input, int length, void* output, int maxout)
       ip += ctrl;
       op += ctrl;
 
-      loop = BLOSCLZ_EXPECT_CONDITIONAL(ip < ip_limit);
+      loop = (uint32_t)BLOSCLZ_EXPECT_CONDITIONAL(ip < ip_limit);
       if(loop)
         ctrl = *ip++;
     }
diff --git a/src/cseqdb.cpp b/src/cseqdb.cpp
new file mode 100644
index 0000000..b84f86c
--- /dev/null
+++ b/src/cseqdb.cpp
@@ -0,0 +1,48 @@
+#include <string.h>
+#include <vector>
+#include "seqdb.h"
+#include "cseqdb.h"
+
+static std::vector<SeqDB*> dbs;
+static std::vector<Sequence*> seqs;
+
+seqdb_file_t seqdb_open(const char* filename)
+{
+	seqdb_file_t ret = dbs.size();
+	dbs.push_back(SeqDB::open(filename));
+	seqs.push_back(new Sequence());
+	return ret;
+}
+
+void seqdb_close(seqdb_file_t f)
+{
+	delete dbs[f];
+	delete seqs[f];
+}
+
+int seqdb_next_record(seqdb_file_t f)
+{
+	return (int)dbs[f]->read(*seqs[f]);
+}
+
+size_t seqdb_id_len(seqdb_file_t f)
+{
+	return seqs[f]->idline.size();
+}
+
+void seqdb_id(seqdb_file_t f, char* id)
+{
+	size_t len = seqs[f]->idline.size();
+	strncpy(id, seqs[f]->idline.data(), len);
+}
+
+size_t seqdb_seq_len(seqdb_file_t f)
+{
+	return seqs[f]->seq.size();
+}
+
+void seqdb_seq(seqdb_file_t f, char* seq)
+{
+	size_t len = seqs[f]->seq.size();
+	strncpy(seq, seqs[f]->seq.data(), len);
+}
diff --git a/src/cseqdb.h b/src/cseqdb.h
new file mode 100644
index 0000000..142631f
--- /dev/null
+++ b/src/cseqdb.h
@@ -0,0 +1,19 @@
+#include <stdlib.h>
+#include <stdint.h>
+
+typedef uint32_t seqdb_file_t;
+
+#ifdef __cplusplus
+#define EXT extern "C"
+#else
+#define EXT  
+#endif
+
+EXT seqdb_file_t seqdb_open(const char* filename);
+EXT void seqdb_close(seqdb_file_t f);
+EXT int seqdb_next_record(seqdb_file_t f);
+EXT size_t seqdb_id_len(seqdb_file_t f);
+EXT void seqdb_id(seqdb_file_t f, char* id);
+EXT size_t seqdb_seq_len(seqdb_file_t f);
+EXT void seqdb_seq(seqdb_file_t f, char* seq);
+
diff --git a/src/fastq.cpp b/src/fastq.cpp
index caca8b7..a98139d 100644
--- a/src/fastq.cpp
+++ b/src/fastq.cpp
@@ -46,12 +46,6 @@ FASTQ::FASTQ(const char* filename)
 	line.reserve(128);
 }
 
-FASTQ::~FASTQ() {}
-bool FASTQ::good() { return true; }
-bool FASTQ::next(Sequence& seq) { return true; }
-void FASTQ::next_line(string& buf) {}
-bool FASTQ::next_line(char* buf, size_t count) { return true; }
-
 void FASTQ::check_delim(char delim)
 {
 	if (delim != FASTQ_DELIM)
@@ -99,7 +93,7 @@ mmapFASTQ::~mmapFASTQ()
 	if (errno) PERROR("could not close file '" << filename << "'")
 }
 
-bool mmapFASTQ::good() { return (nchar <= size); }
+bool mmapFASTQ::good() { return (nchar < size); }
 
 bool mmapFASTQ::next(Sequence& seq)
 {
@@ -119,7 +113,7 @@ bool mmapFASTQ::next(Sequence& seq)
 	/* Quality line. */
 	next_line(seq.qual);
 
-	return (nchar <= size);
+	return (nchar < size);
 }
 
 void mmapFASTQ::next_line(string& buf)
@@ -136,7 +130,7 @@ void mmapFASTQ::next_line(string& buf)
 
 bool mmapFASTQ::next_line(char* buf, size_t count)
 {
-	if (nchar + count + 1 > size) return false;
+	//if (nchar + count + 1 > size) return false;
 	char* start = input + nchar;
 	char* c = (char*)memchr(start, '\n', count + 1);
 	if (c == NULL)
@@ -146,7 +140,7 @@ bool mmapFASTQ::next_line(char* buf, size_t count)
 	memcpy(buf, start, n - 1);
 	nchar += n;
 	nline++;
-	return true;
+	return (nchar <= size);
 }
 
 streamFASTQ::streamFASTQ(const char* filename) : FASTQ(filename)
@@ -203,6 +197,6 @@ bool streamFASTQ::next_line(char* buf, size_t count)
 		      " has more than " << count << " characters")
 	memcpy(buf, line.data(), line.size());
 	nline++;
-	return true;
+	return input->good();
 }
 
diff --git a/src/fastq.h b/src/fastq.h
index 7d96e2d..b7ce4c1 100644
--- a/src/fastq.h
+++ b/src/fastq.h
@@ -9,11 +9,10 @@ class FASTQ
 {
 	public:
 		FASTQ(const char* filename);
-		virtual ~FASTQ();
-		virtual bool good();
-		virtual bool next(Sequence& seq);
-		virtual void next_line(std::string& buf);
-		virtual bool next_line(char* buf, size_t count);
+		virtual bool good() = 0;
+		virtual bool next(Sequence& seq) = 0;
+		virtual void next_line(std::string& buf) = 0;
+		virtual bool next_line(char* buf, size_t count) = 0;
 		void check_delim(char delim);
 		void check_plus();
 
diff --git a/src/h5seqdb.cpp b/src/h5seqdb.cpp
index 3bc7402..e9cae14 100644
--- a/src/h5seqdb.cpp
+++ b/src/h5seqdb.cpp
@@ -35,6 +35,8 @@
 #define PROGNAME "seqdb"
 #include "util.h"
 
+#define BLOSC_LEVEL 4
+
 #ifndef H5SEQDB_MALLOC_ALIGN
 #define H5SEQDB_MALLOC_ALIGN 64
 #endif
@@ -123,11 +125,43 @@ void H5SeqDB::open_datasets(const char* path)
 	ilen = idims[1];
 	NOTIFY("sequence length is " << slen << ", ID length is " << ilen)
 
+	/* Read SeqPack tables from attributes. */
+	uint8_t enc[SEQPACK_ENC_SIZE];
+	read_array_attribute(H5SEQDB_ENC_BASE_ATTR,
+			H5T_NATIVE_UINT8, SEQPACK_ENC_SIZE, enc);
+	pack->setEncBase(enc, SEQPACK_ENC_SIZE);
+	read_array_attribute(H5SEQDB_ENC_QUAL_ATTR,
+			H5T_NATIVE_UINT8, SEQPACK_ENC_SIZE, enc);
+	pack->setEncQual(enc, SEQPACK_ENC_SIZE);
+
+	char dec[SEQPACK_DEC_SIZE];
+	read_array_attribute(H5SEQDB_DEC_BASE_ATTR,
+			H5T_NATIVE_CHAR, SEQPACK_DEC_SIZE, dec);
+	pack->setDecBase(dec, SEQPACK_DEC_SIZE);
+	read_array_attribute(H5SEQDB_DEC_QUAL_ATTR,
+			H5T_NATIVE_CHAR, SEQPACK_DEC_SIZE, dec);
+	pack->setDecQual(dec, SEQPACK_DEC_SIZE);
+
 	/* Cleanup. */
 	//H5TRY(H5Pclose(dapl))
 	H5TRY(H5Gclose(group))
 }
 
+void H5SeqDB::read_array_attribute(
+		const char* name,
+		hid_t type,
+		hsize_t n,
+		void* array)
+{
+	hid_t attr = H5Aopen(h5file, name, H5P_DEFAULT);
+	H5CHK(attr)
+
+	H5TRY(H5Aread(attr, type, array))
+
+	/* Cleanup. */
+	H5TRY(H5Aclose(attr))
+}
+
 void H5SeqDB::create_file(const char* filename, hid_t mode)
 {
 	/* Property lists. */
@@ -160,7 +194,7 @@ void H5SeqDB::create_datasets(const char* path)
 
 	/* BLOSC compression. */
 	unsigned cd[6];
-	cd[4] = 4;
+	cd[4] = BLOSC_LEVEL;
 	cd[5] = 0;
 	H5TRY(H5Pset_filter(idcpl, FILTER_BLOSC, H5Z_FLAG_OPTIONAL, 6, cd))
 	H5TRY(H5Pset_filter(sdcpl, FILTER_BLOSC, H5Z_FLAG_OPTIONAL, 6, cd))
diff --git a/src/h5seqdb.h b/src/h5seqdb.h
index 2772849..22dc507 100644
--- a/src/h5seqdb.h
+++ b/src/h5seqdb.h
@@ -22,6 +22,7 @@ class H5SeqDB : public SeqDB {
 		/* functions */
 		void open_file(const char* filename, hid_t mode);
 		void open_datasets(const char* path);
+		void read_array_attribute(const char* name, hid_t type, hsize_t n, void* array);
 		void create_file(const char* filename, hid_t mode);
 		void create_datasets(const char* path);
 		void write_array_attribute(const char* name, hid_t type, hsize_t n, const void* array);
diff --git a/src/seqdb-compress.cpp b/src/seqdb-compress.cpp
index d3da7c5..3f890b4 100644
--- a/src/seqdb-compress.cpp
+++ b/src/seqdb-compress.cpp
@@ -26,6 +26,7 @@
 
 #include <stdlib.h>
 #include <string.h>
+#include <unistd.h>
 #include <iostream>
 #include <vector>
 #include "seqdb.h"
diff --git a/src/seqdb-extract.cpp b/src/seqdb-extract.cpp
index 7747518..b2529ff 100644
--- a/src/seqdb-extract.cpp
+++ b/src/seqdb-extract.cpp
@@ -26,6 +26,7 @@
 
 #include <stdlib.h>
 #include <stdio.h>
+#include <unistd.h>
 #include <iostream>
 #include <vector>
 #include "seqdb.h"
@@ -38,20 +39,28 @@ using namespace std;
 void print_usage()
 {
 	cout << "\n"
-"usage: "PROGNAME" SEQDB\n"
+"usage: "PROGNAME" [-i ID] SEQDB\n"
 "\n"
 "Converts the SEQDB input file to FASTQ format, printing to stdout.\n"
 "\n"
+"  -i  extract only the record at the 1-based index ID\n"
+"\n"
 "Example usage:\n"
 PROGNAME" 1.seqdb >1.fastq\n"
+PROGNAME" -i 1705 1.seqdb\n"
 	<< endl;
 }
 
 int main(int argc, char** argv)
 {
+	size_t id = 0;
+
 	int c;
-	while ((c = getopt(argc, argv, "hv")) != -1)
+	while ((c = getopt(argc, argv, "i:hv")) != -1)
 		switch (c) {
+			case 'i':
+				id = (size_t)strtoul(optarg, NULL, 10);
+				break;
 			case 'v':
 				PRINT_VERSION
 				return EXIT_SUCCESS;
@@ -67,10 +76,17 @@ int main(int argc, char** argv)
 	/* Open the input. */
 	SeqDB* db = SeqDB::open(argv[optind]);
 
-	ios_base::sync_with_stdio(false);
-	setvbuf(stdout, NULL, _IOFBF, 1024*1024);
-
-	db->exportFASTQ(stdout);
+	if (id > 0) {
+		/* Read a single record */
+		Sequence seq;
+		db->readAt(id, seq);
+		cout << seq;
+	} else {
+		/* Dump all records, and adjust stdout buffer for efficiency */
+		ios_base::sync_with_stdio(false);
+		setvbuf(stdout, NULL, _IOFBF, 1024*1024);
+		db->exportFASTQ(stdout);
+	}
 
 	/* Cleanup. */
 	delete db;
diff --git a/src/seqdb-profile.cpp b/src/seqdb-profile.cpp
index ccbbcc0..9d02827 100644
--- a/src/seqdb-profile.cpp
+++ b/src/seqdb-profile.cpp
@@ -26,6 +26,7 @@
 
 #include <stdlib.h>
 #include <stdio.h>
+#include <unistd.h>
 #include <iostream>
 #include <vector>
 #include "seqdb.h"
@@ -51,6 +52,7 @@ void profile(FASTQ* input)
 {
 	vector<size_t> ilen(101, 0);
 	vector<size_t> slen(101, 0);
+	vector<size_t> qval(95, 0);
 	Sequence seq;
 
 	size_t n = 0;
@@ -67,6 +69,13 @@ void profile(FASTQ* input)
 		if (s != q) ERROR("mismatching sequence and quality size at read " << n)
 		if (s >= slen.size()) slen.resize(s+1, 0);
 		slen[s]++;
+
+		for (size_t j=0; j<s; j++) {
+			int qual = (int)seq.qual.at(j);
+			if (qual < 33 || qual > 127)
+				ERROR("non-phred33 quality score at read " << n)
+			qval[qual - 33]++;
+		}
 	}
 
 	fprintf(stderr, "  total reads: %12lu\n", n);
@@ -79,6 +88,10 @@ void profile(FASTQ* input)
 	for (size_t i=0; i<slen.size(); i++)
 		if (slen[i] > 0)
 			fprintf(stderr, "         %5lu %12lu\n", i, slen[i]);
+	cerr << "  phred33 quality scores:" << endl;
+	for (size_t i=0; i<qval.size(); i++)
+		if (qval[i] > 0)
+			fprintf(stderr, "         %5lu %12lu\n", i, qval[i]);
 }
 
 int main(int argc, char** argv)
diff --git a/src/seqdb-version.h.in b/src/seqdb-version.h.in
new file mode 100644
index 0000000..44c02dd
--- /dev/null
+++ b/src/seqdb-version.h.in
@@ -0,0 +1,6 @@
+#ifndef __SEQDB_VERSION_H__
+#define __SEQDB_VERSION_H__
+
+#define SEQDB_VERSION @PACKAGE_VERSION@
+
+#endif
diff --git a/src/seqdb.cpp b/src/seqdb.cpp
index fb48a7d..71c78b9 100644
--- a/src/seqdb.cpp
+++ b/src/seqdb.cpp
@@ -78,9 +78,3 @@ SeqDB::~SeqDB()
 	delete pack;
 }
 
-void SeqDB::write(const Sequence&) {}
-bool SeqDB::read(Sequence&) {}
-void SeqDB::readAt(size_t, Sequence&) {}
-void SeqDB::importFASTQ(FASTQ*) {}
-void SeqDB::exportFASTQ(FILE*) {}
-
diff --git a/src/seqdb.h b/src/seqdb.h
index e0ce9b0..5d39282 100644
--- a/src/seqdb.h
+++ b/src/seqdb.h
@@ -3,6 +3,7 @@
 
 #include <stdio.h>
 #include <string>
+#include "seqdb-version.h"
 #include "seqpack.h"
 #include "seq.h"
 #include "fastq.h"
@@ -20,11 +21,11 @@ class SeqDB {
 
 		/* virtual functions */
 		virtual ~SeqDB();
-		virtual void write(const Sequence& seq);
-		virtual bool read(Sequence& seq);
-		virtual void readAt(size_t i, Sequence& seq);
-		virtual void importFASTQ(FASTQ* f);
-		virtual void exportFASTQ(FILE* f);
+		virtual void write(const Sequence& seq) = 0;
+		virtual bool read(Sequence& seq) = 0;
+		virtual void readAt(size_t i, Sequence& seq) = 0;
+		virtual void importFASTQ(FASTQ* f) = 0;
+		virtual void exportFASTQ(FILE* f) = 0;
 
 		/* data */
 		static const char READ = 0x00;
diff --git a/src/seqdb.pc.in b/src/seqdb.pc.in
new file mode 100644
index 0000000..3d5d205
--- /dev/null
+++ b/src/seqdb.pc.in
@@ -0,0 +1,11 @@
+prefix=@prefix@
+exec_prefix=@exec_prefix@
+libdir=@libdir@
+includedir=@includedir@
+
+Name: SeqDB
+Description: High-throughput sequence data compressor
+Requires: 
+Version: @PACKAGE_VERSION@
+Libs: -L${libdir} -lseqdb @HDF5_LDFLAGS@
+Cflags: -I${includedir}/seqdb @HDF5_CPPFLAGS@
diff --git a/src/seqpack.cpp b/src/seqpack.cpp
index 1696dd4..14021e1 100644
--- a/src/seqpack.cpp
+++ b/src/seqpack.cpp
@@ -53,21 +53,45 @@ SeqPack::SeqPack(int length)
 		if (j < 0 || j > 127) ERROR("internal error building encoding table")
 		enc_base[j] = i;
 	}
-	for (uint8_t i=0; i<=50; i++) {
-		int j = (int)quals[i];
+	for (uint8_t i=1; i<=51; i++) {
+		int j = (int)quals[i-1];
 		if (j < 0 || j > 127) ERROR("internal error building encoding table")
 		enc_qual[j] = i;
 	}
 
 	/* create decoding tables */
 	for (int i=0; i<=4; i++) {
-		for (int j=0; j<=50; j++) {
+		for (int j=1; j<=51; j++) {
 			dec_base[i*51 + j] = bases[i];
-			dec_qual[i*51 + j] = quals[j];
+			dec_qual[i*51 + j] = quals[j-1];
 		}
 	}
 }
 
+void SeqPack::setEncBase(const uint8_t* _enc_base, size_t len)
+{
+	if (len != SEQPACK_ENC_SIZE) ERROR("incorrect size for encoding table")
+	memcpy(enc_base, _enc_base, SEQPACK_ENC_SIZE);
+}
+
+void SeqPack::setEncQual(const uint8_t* _enc_qual, size_t len)
+{
+	if (len != SEQPACK_ENC_SIZE) ERROR("incorrect size for encoding table")
+	memcpy(enc_qual, _enc_qual, SEQPACK_ENC_SIZE);
+}
+
+void SeqPack::setDecBase(const char* _dec_base, size_t len)
+{
+	if (len != SEQPACK_DEC_SIZE) ERROR("incorrect size for decoding table")
+	memcpy(dec_base, _dec_base, SEQPACK_DEC_SIZE);
+}
+
+void SeqPack::setDecQual(const char* _dec_qual, size_t len)
+{
+	if (len != SEQPACK_DEC_SIZE) ERROR("incorrect size for decoding table")
+	memcpy(dec_qual, _dec_qual, SEQPACK_DEC_SIZE);
+}
+
 void SeqPack::pack(
 		const char* seq,
 		const char* qual,
diff --git a/src/seqpack.h b/src/seqpack.h
index a6a683b..35d4366 100644
--- a/src/seqpack.h
+++ b/src/seqpack.h
@@ -16,6 +16,10 @@ class SeqPack {
 		const uint8_t* getEncQual() { return enc_qual; }
 		const char* getDecBase() { return dec_base; }
 		const char* getDecQual() { return dec_qual; }
+		void setEncBase(const uint8_t* _enc_base, size_t len);
+		void setEncQual(const uint8_t* _enc_qual, size_t len);
+		void setDecBase(const char* _dec_base, size_t len);
+		void setDecQual(const char* _dec_qual, size_t len);
 		void pack(const char* seq, const char* qual, uint8_t* record);
 		void parpack(size_t n, const char* src, uint8_t* dst);
 		void unpack(const uint8_t* record, char* seq, char* qual);
diff --git a/src/shuffle.c b/src/shuffle.c
index 8231433..a103317 100644
--- a/src/shuffle.c
+++ b/src/shuffle.c
@@ -25,7 +25,7 @@
 
 /* Shuffle a block.  This can never fail. */
 void _shuffle(size_t bytesoftype, size_t blocksize,
-	      uint8_t* _src, uint8_t* _dest) {
+      uint8_t* _src, uint8_t* _dest) {
   size_t i, j, neblock, leftover;
 
   /* Non-optimized shuffle */
@@ -41,7 +41,7 @@ void _shuffle(size_t bytesoftype, size_t blocksize,
 
 /* Unshuffle a block.  This can never fail. */
 void _unshuffle(size_t bytesoftype, size_t blocksize,
-		uint8_t* _src, uint8_t* _dest) {
+      uint8_t* _src, uint8_t* _dest) {
   size_t i, j, neblock, leftover;
 
   /* Non-optimized unshuffle */
diff --git a/src/shuffle.h b/src/shuffle.h
index 485e7ac..fd8ff81 100644
--- a/src/shuffle.h
+++ b/src/shuffle.h
@@ -9,17 +9,8 @@
 
 /* Shuffle/unshuffle routines */
 
-#ifdef __cplusplus
-extern "C" {
-#endif
-
 void shuffle(size_t bytesoftype, size_t blocksize,
              unsigned char* _src, unsigned char* _dest);
 
 void unshuffle(size_t bytesoftype, size_t blocksize,
                unsigned char* _src, unsigned char* _dest);
-
-#ifdef __cplusplus
-}
-#endif
-
diff --git a/src/util.h b/src/util.h
index 8897b72..3a938ff 100644
--- a/src/util.h
+++ b/src/util.h
@@ -25,26 +25,6 @@
 	if (buf == NULL) ERROR("out of memory at " << __FILE__ << ":" << __LINE__)\
 }while(0);
 
-#if LIKWID
-#define TIC(label) likwid_markerStartRegion(STRINGIFY(label));
-#define TOC(label) likwid_markerStopRegion(STRINGIFY(label));
-#else
-#define TIC(label)\
-	double label ;\
-	do{\
-		struct timeval tv;\
-		gettimeofday(&tv, NULL);\
-		label = tv.tv_sec + 0.000001 * tv.tv_usec;\
-	}while(0);
-#define TOC(label)\
-	do{\
-		struct timeval tv;\
-		gettimeofday(&tv, NULL);\
-		label = tv.tv_sec + 0.000001 * tv.tv_usec - label ;\
-	}while(0);\
-	printf(STRINGIFY(label) "\t%g\n", label );
-#endif
-
 #define PRINT_VERSION cout << "SeqDB " << PACKAGE_VERSION << endl;
 
 #endif
